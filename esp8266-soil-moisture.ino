#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define moisturePin A0

#define HUB 1
#define FLASH 0
#define MULTIPLIER 3

const char* ssid = "SoftNetworkAP";
const char* password = "SoftNetworkPassword";
const char ip[] = "192.168.4.1";
unsigned int port = 3000;
WiFiUDP udp;

const char location[] = "kitchen";
const char plant[] = "leaves";
const char pot[] = "glass";

void setup() {
  pinMode(moisturePin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    ESP.restart();
  }

  Serial.println("Connected");

  udp.begin(port);

  ArduinoOTA.onStart([]() {
    flash(1);
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    flash(1);
  });

  ArduinoOTA.onEnd([]() {
    flash(5);
  });

  ArduinoOTA.onError([](ota_error_t error) {
    if (error == OTA_AUTH_ERROR) {
      flash(1);
    } else if (error == OTA_BEGIN_ERROR) {
      flash(1);
    } else if (error == OTA_CONNECT_ERROR) {
      flash(1);
    } else if (error == OTA_RECEIVE_ERROR) {
      flash(1);
    } else if (error == OTA_END_ERROR) {
      flash(1);
    }
  });

  ArduinoOTA.begin();
}

void loop() {
  ArduinoOTA.handle();
  getMoisture();
  if (HUB) {
    sendSelf();
  }
  delay(1000);
}

void flash(unsigned int count) {
  if (FLASH) {
    for (int i = 0; i < count; i++) {
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
    }
  }
}

void sendSelf() {
  if (FLASH) {
    digitalWrite(LED_BUILTIN, LOW);
  }

  udp.beginPacket(ip, port);
  char info[100];
  sprintf(info, "hub:%s:%s", location, WiFi.localIP().toString().c_str());
  udp.write(info);
  udp.endPacket();

  if (FLASH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

void getMoisture() {
  if (FLASH) {
    digitalWrite(LED_BUILTIN, LOW);
  }
  unsigned int moisture = (1024 - analogRead(moisturePin)) / MULTIPLIER;
  udp.beginPacket(ip, port);
  char info[100];
  sprintf(info, "moisture._t_location.%s._t_plant.%s._t_pot.%s:%u|c", location, plant, pot, moisture);
  udp.write(info);
  udp.endPacket();
  if (FLASH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}
