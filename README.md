# ESP8266 Soil Moisture

## Measure soil moisture and send to hub

Uses ESP8266-hub, but can also be standalone.

### Purpose

Reads the analog signal over A0 (a soil moisture sensor, in this case), formats that information for statsd consumption, then sends data over UDP.

### Setup

1. Obtain necessary hardware
2. `git clone`
3. Replace necessary variables
4. Upload and provide power :)
5. May be necessary to click the "reset" button after uploading

### Testing

1. Connect to network
2. See if metrics are being sent/received (the hub makes this easy by checking the serial monitor output)

### In Progress
